# Chiffrer ses requêtes DNS, vie privée et la sécurité

Pour faire suite à un post récent [ici](https://framasphere.org/posts/10587860) je vous mets un tuto ~~succinct~~ pour chiffrer vos requêtes DNS depuis votre ordinateur.

*Avertissement ce tuto est fait pour les utilisateurs de Archlinux et dérivés. Si vous êtes sur debian ou dérivées qui utilisent Systemd, vous pouvez suivre ce tuto en adaptant les chemins si nécessaires.*

Nous allons voir

1. Pourquoi modifier les paramètres par défaut des requête DNS de son ordi ?

2. Installer et paramétrer unbound

3. Installer et paramétrer DNScrypt


# Pourquoi modifier les paramètres par défaut des requêtes DNS de son ordi ?

Par défaut, si nous n'utilisons pas de VPN, les requêtes DNS sont effectuées chez notre Fournisseur d'Accès Internet comme l'a rappelé Djan Gicquel sur framasphere récemment.

Ainsi quand nous souhaitons accéder à un site (ex: https://www.wikimedia.org/), le navigateur va interroger le résolveur de DNS pour connaître l'adresse IP où se trouve le site web wikimedia. Le résolveur de notre FAI nous répond en nous donnant l'adresse IP du site wikimedia pour que notre navigateur trouve sa route jusqu'au serveur qui héberge le site web. Mais nous pouvons tout autant éviter de passer par le résolveur de notre FAI et choisir un qui respecte plus la vie privée, notamment en ne conservant aucun log.

Par exemple; nous voyons ce qui se passe à la ligne 10 de la fenêtre ci-dessous. Mon ordi (dont l'adresse sur le réseau local est 192.168.0.50) lance une requête au résolveur de DNS se situant à l'adresse 208.67.220.220.

[![https://imgur.com/4F367Ob.png](https://imgur.com/4F367Ob.png)](https://imgur.com/4F367Ob.png)

Mon ordi n'utilise donc plus le résolveur de mon FAI mais celui que j'ai renseigné dans les paramètres de Network-Manager: l'adresse **208.67.220.220** est saisie à la ligne Server DNS et la méthode est "Manuel".

[![Network-Manager](https://imgur.com/FQHAFiX.png)]

Avec cette solution, nous n'interrogeons plus le résolveur DNS de mon FAI donc il ne peut enregistrer plus directement toutes les requête DNS que nous réalisons. Ceci constitue une tout petit pas de protection de la vie privée.

Cependant nous pouvons voir que notre requêtes DNS adressée au résolveur 208.67.220.220 passe en clair (voir la ligne 10) :
~~~
"Standard query 0x1c5d A www.wikimedia.org OPT"
~~~

Bref, si mon FAI ou autre entité souhaite enregistrer nos requêtes; il peut le faire en sniffant le réseau facilement.

Pour se prémunir cela, nous pouvons chiffrer vos requêtes DNS pour ne pas offrir facilement notre historique de navigation à notre FAI, il y a plusieurs méthodes. Nous allons voir l'une d'entre elles qui consiste à chiffrer ses requêtes DNS à l'aide de DNSCrypt et à utiliser un cache local Unbound pour plus de rapidité.

Bien sûr les deux solutions sont indépendantes, vous pouvez configurer un cache local comme Unbound pour accélérer les requêtes redondantes sans pour autant utiliser DNScrypt. Par ailleurs, DNScrypt peut être configuré avec son propre cache mais c'est une solution que nous n'arborderons pas ici. 

Nous allons voir dans un premier temps comment installer et configurer Unbound tout seul, puis avec le protocole DNNSEC, enfin dans un dernier temps avec DNScrypt.


# Installation et configuration de Unbound

Unbound est un cache simple et léger qui conserve vos requêtes DNS. Cela évite tout simplement à réinterroger un résolveur DNS sur le réseau lorsque vous avez déjà consulter le site il y a quelques minutes. L'effet direct est un chargement plus rapide de la page.

## Configuration avec Unbound seul

L'installation se fait par la commande
~~~bash
sudo pacman -S unbound
~~~

Il convient de spécifier à quel résolveur DNS nous souhaitons que Unbound se connecte. Dans le but de préserver sa vie privée, il serait peu pertinent d'aller interroger des résolveur de **MéchantsPasBoVilains** tel que Google ou Cloudfare ... (C'est notamment pour cela que la solution choisie par Mozilla pour faire du DoH est [décevante](https://www.passwordrevelator.net/blog/le-dns-over-https-doh-de-firefox-serait-il-mauvais/)) *Parenthèse fermée*.

Nous allons choisir quelques résolveurs qui s'engagent à ne conserver aucun log et qui sont les plus proches de nous. Premièrement cela évite à ce qu'un tiers conserve notre historique de navigation et deuxièmement plus le résolveur est proche plus la réponse est rapide et la navigation sera réactive. Pour cela on peut faire un test automatique auprès du site https://www.opennic.org/ ou en mode manuel https://servers.opennicproject.org/.

Nous avons choisi 3 serveurs via Opennic project et deux de la [FDN](https://isengard.fdn.fr/).

Maintenant, il faut ajouter ces serveurs dans le fichier de configuration de Unbound. Mais tout d'abord, nous sauvegardons notre fichier de conf. Nous pourrons revenir en arrière en cas de problème.
~~~bash
sudo cp /etc/unbound/unbound.conf /etc/unbound/unbound.conf_back
~~~
Nous pouvons éditer le fichier de conf
~~~bash
sudo mousepad /etc/unbound/unbound.conf
~~~

Nous ajoutons les résolveurs choisis dans le fichier Unbound.conf. (respecter l'identation)
~~~bash
forward-zone:
  name: "."
  forward-addr: 109.230.224.42  # orsn-ns.godau.eu
  forward-addr: 80.67.169.12  # serveur de FDN 2001:910:800::12
  forward-addr: 80.67.169.40  # serveur de FDN
  forward-addr: 208.67.222.222   #serveur DNS OpenDNS
  forward-addr: 208.67.220.220  #serveur DNS OpenDNS
~~~

Il est nécessaire de télécharger un fichier contenant les noms et adresses des serveurs racine DNS. [Quezako ?](https://fr.wikipedia.org/wiki/Serveur_racine_du_DNS)
~~~bash
sudo curl --output /etc/unbound/root.hints https://www.internic.net/domain/named.cache
~~~

Ensuite il faut spécifier l'emplacement de ce fichier dans le fichier de conf de Unbound à la section **server:**

~~~bash
server:
  root-hints: "/etc/unbound/root.hints"
~~~

Dernière étape, si on veut faire certaines opération de contrôle de Unbound (comme vérifier ce que nous avons dans son cache ou vider son cache), il est nécessaire d'activer cette option. On le fait par la commande:
~~~bash
 sudo unbound-control-setup
~~~
Cela a pour effet de créer des certificats et clefs privées. Il reste à confirmer cette option en ajoutant les lignes suivantes dans le fichier de configuration de Unbound
~~~bash
remote-control:
    # Enable remote control with unbound-control(8) here.
    # set up the keys and certificates with unbound-control-setup.
    control-enable: yes

    # what interfaces are listened to for remote control.
    # give 0.0.0.0 and ::0 to listen to all interfaces.
    control-interface: 127.0.0.1

    # port number for remote control operations.
    control-port: 8953

    # unbound server key file.
    server-key-file: "/etc/unbound/unbound_server.key"

    # unbound server certificate file.
    server-cert-file: "/etc/unbound/unbound_server.pem"

    # unbound-control key file.
    control-key-file: "/etc/unbound/unbound_control.key"

    # unbound-control certificate file.
    control-cert-file: "/etc/unbound/unbound_control.pem"
~~~

Nous enregistrons le fichier `unbound.conf` et redémarrons le service:
~~~bash
 sudo systemctl restart unbound.service
~~~
Dorénavant, nous pouvons vérifier par exemple les adresses IP en cache dans Unbound par:
~~~bash
 sudo unbound-control dump_cache
~~~
Ou encore vider le cache au complet:
~~~bash
 sudo unbound-control reload
~~~

## Ajout du protocole DNNSEC
Nous devons installer une application qui gérera ce protocole:

`sudo pacman -S dnssec-anchors`

Puis nous ajoutons une ligne pour préciser l'emplacement des clefs à la section **server:** du fichier de conf.

~~~bash
server:
  trust-anchor-file: /etc/unbound/trusted-key.key
~~~
Nous sauvegardons le fichier et nous relançons Unbound
~~~bash
sudo systemctl restart unbound.service
~~~
Nous pouvons vérifier si le service Unbound fonctionne correctement:
~~~bash
sudo systemctl status unbound.service
● unbound.service - Validating, recursive, and caching DNS resolver
     Loaded: loaded (/usr/lib/systemd/system/unbound.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2020-11-19 15:55:11 CET; 3s ago
       Docs: man:unbound(8)
   Main PID: 6414 (unbound)
      Tasks: 2 (limit: 9398)
     Memory: 4.8M
     CGroup: /system.slice/unbound.service
             └─6414 /usr/bin/unbound -d -p

Nov 19 15:55:11 user-lapuntu systemd[1]: Starting Validating, recursive, and caching DNS resolver...
Nov 19 15:55:11 user-lapuntu unbound[6414]: [6414:0] notice: init module 0: subnet
Nov 19 15:55:11 user-lapuntu unbound[6414]: [6414:0] notice: init module 1: validator
Nov 19 15:55:11 user-lapuntu unbound[6414]: [6414:0] notice: init module 2: iterator
Nov 19 15:55:11 user-lapuntu unbound[6414]: [6414:0] info: start of service (unbound 1.11.0).
Nov 19 15:55:11 user-lapuntu systemd[1]: Started Validating, recursive, and caching DNS resolver.
~~~

Nous allons vérifier que le protocole DNNSEC fonctionne correctement en laçant la commande suivante :
~~~
sudo dig @localhost doh.bortzmeyer.fr +dnssec | grep -A1 HEADER
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 60830
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 1
~~~
Ici nous avons le flag **ad** qui signifie "authenticated data". La signature du résolveur doh.bortzmeyer.fr a été authentifiée. En cas d'absence du drapeau **ad** le problème peut venir de plusieurs causes. Pour plus de détails [ici](https://www.redpill-linpro.com/techblog/2019/08/27/evaluating-local-dnssec-validators.html#private-domains-and-negative-trust-anchors) et [là](https://www.bortzmeyer.org/unbound.html)

### Problème de mémoire:
Sur ma configuration, le statut Unbound a fait remonter des erreurs. Cela ressemblait à ceci:

~~~bash
sudo systemctl status unbound.service
● unbound.service - Validating, recursive, and caching DNS resolver
     Loaded: loaded (/usr/lib/systemd/system/unbound.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2020-11-19 15:52:15 CET; 2s ago
       Docs: man:unbound(8)
   Main PID: 6318 (unbound)
      Tasks: 2 (limit: 9398)
     Memory: 4.9M
     CGroup: /system.slice/unbound.service
             └─6318 /usr/bin/unbound -d -p

Nov 19 15:52:15 user-lapuntu unbound[6318]: [1605797535] unbound[6318:0] warning: so-sndbuf 4194304 was not granted. Got 425984. To fix: start with root permissions(linux) or sysctl bigge>
Nov 19 15:52:15 user-lapuntu unbound[6318]: [1605797535] unbound[6318:0] warning: so-rcvbuf 4194304 was not granted. Got 425984. To fix: start with root permissions(linux) or sysctl bigge>
Nov 19 15:52:15 user-lapuntu unbound[6318]: [1605797535] unbound[6318:0] warning: so-sndbuf 4194304 was not granted. Got 425984. To fix: start with root permissions(linux) or sysctl bigge>
Nov 19 15:52:15 user-lapuntu unbound[6318]: [1605797535] unbound[6318:0] warning: so-rcvbuf 4194304 was not granted. Got 425984. To fix: start with root permissions(linux) or sysctl bigge>
Nov 19 15:52:15 user-lapuntu unbound[6318]: [1605797535] unbound[6318:0] warning: so-sndbuf 4194304 was not granted. Got 425984. To fix: start with root permissions(linux) or sysctl bigge>
Nov 19 15:52:15 user-lapuntu unbound[6318]: [6318:0] notice: init module 0: subnet
Nov 19 15:52:15 user-lapuntu unbound[6318]: [6318:0] notice: init module 1: validator
Nov 19 15:52:15 user-lapuntu unbound[6318]: [6318:0] notice: init module 2: iterator
Nov 19 15:52:15 user-lapuntu unbound[6318]: [6318:0] info: start of service (unbound 1.11.0).
Nov 19 15:52:15 user-lapuntu systemd[1]: Started Validating, recursive, and caching DNS resolver.
~~~
Il semblerait que cela soit du à une quantitité de mémoire trop failble allouée à systemd. Nous allons donc augnemter cette valeur. Vous pouvez tester cette solution avec cette commande:
~~~bash
sudo sysctl -w net.core.rmem_max=4194304 && sudo sysctl -w net.core.wmem_max=4194304
~~~
Puis relancer unbound et vérifier son statut à nouveau:
~~~bash
sudo systemctl restart unbound.service && sudo systemctl status unbound.service
~~~
Si vous n'avez plus d'erreur tant mieux, nous pouvons éditer le fichier /etc/sysctl.d/99-sysctl.conf pour ne plus avoir cette erreur au prochain boot en ajoutant ces deux lignes:

~~~bash
net.core.rmem_max=4194304
net.core.wmem_max=4194304
~~~
Pour plus d'[infos](https://wiki.archlinux.org/index.php/Sysctl.conf#Increase_the_memory_dedicated_to_the_network_interfaces).

## Configuration de Network-Manager

Il convient de demander à Network-Manager de ne plus interroger directement le résolveur **208.67.220.220** mais de passer par le service du cache local de unbound. Nous remplaçons la valeur **208.67.220.220** par **127.0.0.1**.

[![Network-Manager-local](https://imgur.com/wp5zzRv.png)]

Dernière étape, il convient de modifier les paramètres de DNS du fichier de configuration resolv.conf et de le protéger comme les modifications faites par Network-Manager. Pour empêcher Network-Manager cette modificaion il y a deux moyens. 

Le premier est d'empêcher la modification du fichier par la commande :
~~~bash
sudo chattr +i /etc/resolv.conf
~~~
Nous pouvons revenir en arrière avec:
~~~bash
sudo chattr -i /etc/resolv.conf
~~~

La seconde méthode est d'utiliser une applicatio **openresolv** qui assure que les paramètres du resolv.conf soient tels que nous le souhaitons à chaque démarrage.

~~~bash
 sudo pacman -S openresolv
 ~~~

 Nous éditons le fichier resolvconf.conf:

 ~~~bash
 sudo mousepad /etc/resolvconf.conf
 ~~~
en copiant ceci
 ~~~bash
# Configuration for resolvconf(8)
# See resolvconf.conf(5) for details

resolv_conf=/etc/resolv.conf
# If you run a local name server, you should uncomment the below line and
# configure your subscribers configuration files below.
#name_servers=127.0.0.1

# If you run a local name server, you should uncomment the below line and
# configure your subscribers configuration files below.
name_servers="::1 127.0.0.1"
options edns0
# private_interfaces="*"
~~~
 Ensuite on applique ces paramètres sur le fichier /etc/resolv.conf en lançant la commande suivante: `sudo resolvconf -u` . À ce stade, nous avons un cache DNS opérationnel, notre ordi se connecte aux résolveurs choisis et leur identité est authentifiée grâce à DNNSEC. Il nous reste à chiffrer toute cette communication avec DNScrypt.


# DNScrypt

DNScrypt nous permet d'une part de chiffrer la requête transmise au résolveur mais assure aussi la signature de la réponse pour assurer l'intégrité du message. Ceci empêche de lire la requête mais éventuellement qu'elle soient modifiées en cours de route. Par contre par défaut l'identité du résolveur n'est pas authentifié. C'est le protocole [DNNSEC](https://fr.wikipedia.org/wiki/Domain_Name_System_Security_Extensions) configuré plus haut avec Unbound qui se charge de l'authentification de l'identité du résolveur. 

## DNScrypt installation:

Debian, ubuntu et dérivé :

`sudo apt install dnscrypt-proxy`

Archlinux, manjaro et dérivé:

`sudo pacman -S dnscrypt-proxy`

Avant toute chose, il vaut mieux sauvegarder nos paramètres :

`sudo cp /etc/dnscrypt-proxy/dnscrypt-proxy.toml /etc/dnscrypt-proxy/dnscrypt-proxy.toml_backup`

Bien sûr si nous obtenons des erreurs nous pouvons revenir à notre configuration originale en remplaçant notre fichier de conf par celui du backup:

`sudo cp /etc/dnscrypt-proxy/dnscrypt-proxy.toml_backup /etc/dnscrypt-proxy/dnscrypt-proxy.toml`



### Configuration de DNScrypt:


#### Liste des résolveurs compatibles DNScrypt

Par défaut, DNScrypt récupère une liste de résolveurs, mise à jour régulièrement, supportant ce protocole sur le net. Attention cette liste contient des [DNS menteurs](https://fr.wikipedia.org/wiki/Manipulation_de_l%27espace_des_noms_de_domaine) qui peuvent nous protéger ou nous restreindre l'accès à certains domaines. Elle contient aussi des résolveurs gérés par des **MéchantsPasBoVilains** comme Cloudfare, Cisco, ou Google pour ne citer que les plus connus.

Par conséquent, il peut être intéressant de restreindre la liste des serveurs sur lesquels notre DNScrypt ira récupérer les adresses IP. Pour cela, il nous faut ouvrir le fichier de configuration dnscrypt-proxy.toml, par exemple `sudo mousepad /etc/dnscrypt-proxy/dnscrypt-proxy.toml`.

Si vous n'avez pas d'adresse précise connue de confiance, vous pouvez piocher dans [la liste](https://dnscrypt.info/public-servers/) régulièrement mise à jour. De même certains résolveurs sont menteurs donc n'hésitez pas à prendre le temps de lire la description ou de faire une recherche sur le net sur la vocation de ces résolveurs.
### Ajout des résolveurs DNS choisis

Par exemple:
À partir de la liste donnée ci-dessus, nous allons prendre les paramètres du serveur IPV4 de ibksturm qui est localisé en Suisse.

~~~bash
dnscrypt-server (nginx - encrypted-dns - unbound backend), DNSSEC / Non-Logged / Uncensored, OpenNIC and Root DNS-Zone Copy Hosted in Switzerland by ibksturm, aka Andreas Ziegler
Protocol 	DNSCrypt
Addresses 	[ "83.78.120.227" ]
Ports 	[ 8443 ]
DNSSEC 	true
No filters 	true
No logs 	true
Stamp 	sdns://AQcAAAAAAAAAEjgzLjc4LjEyMC4yMjc6ODQ0MyDBz1dQALBbwmxiH17PmqJWCs6_AH6-yzp_9LIN4LQ57hgyLmRuc2NyeXB0LWNlcnQuaWJrc3R1cm0
~~~

Les URL que nous choisissons depuis cette liste seront notées dans la section **Servers** puis **[sources]** du fichier de conf de DNScrypt. Pour ce faire, il nous suffit de conserver l'information du sdns et d'y attribuer un nom. Vous pouvez conserver le nom **ibksturm** ou lui donner un autre nom comme **DNS_Static_01**. On garde celui de **ibksturm** pour notre exemple plus loin.

Donc éditons le fichier de conf de DNScrypt:
`sudo mousepad /etc/dnscrypt-proxy/dnscrypt-proxy.toml`

Déplaçons nous à la section **[Static]** pour ajouter les lignes suivantes:
~~~bash
[static]  
  [static.'ibkstrum']
  stamp = 'sdns:AQcAAAAAAAAAEjgzLjc4LjEyMC4yMjc6ODQ0MyDBz1dQALBbwmxiH17PmqJWCs6_AH6-yzp_9LIN4LQ57hgyLmRuc2NyeXB0LWNlcnQuaWJrc3R1cm0'
~~~
*Notez le petit changement d'écriture au niveau du sdns. Les deux barres obliques disparaissent et un signe **=** doit être inséré après le **stamp**.*

Il est possible de renseigner plusieurs serveurs au cas où l'un d'entre eux est indisponible.

~~~bash
[static]  

  [static.'ibkstrum']
  stamp = 'sdns:AQcAAAAAAAAAEjgzLjc4LjEyMC4yMjc6ODQ0MyDBz1dQALBbwmxiH17PmqJWCs6_AH6-yzp_9LIN4LQ57hgyLmRuc2NyeXB0LWNlcnQuaWJrc3R1cm0'

  [static.'bortzmeyer'] # Non-logging DoH server in France operated by Stéphane Bortzmeyer. https://www.bortzmeyer.org/doh-bortzmeyer-fr-policy.html
  stamp = 'sdns:AgcAAAAAAAAADDE5My43MC44NS4xMSA-GhoPbFPz6XpJLVcIS1uYBwWe4FerFQWHb9g_2j24OBFkb2guYm9ydHptZXllci5mcgEv'

  [static.'cryptostorm'] #  Switzerland DNSCrypt server provided by https://cryptostorm.is/
  stamp = 'sdns:AQIAAAAAAAAACzgxLjE3LjMxLjM0IDEzcq1ZVjLCQWuHLwmPhRvduWUoTGy-mk8ZCWQw26laHjIuZG5zY3J5cHQtY2VydC5jcnlwdG9zdG9ybS5pcw'

  [static.'DNS_opennic-R4SAS']
  stamp = 'sdns:AQcAAAAAAAAADTE1MS44MC4yMjIuNzkgqdYyOk8lgAkmGXUVAs4jHh922d53bIfGu7KKDv_bDk4gMi5kbnNjcnlwdC1jZXJ0Lm9wZW5uaWMuaTJwZC54eXo'
~~~
Il nous reste à préciser à DNScrypt de ne communiquer uniquement avec ces résolveurs afin d'éviter qu'il aille chercher d'autres résolveurs de la liste que DNScrypt récupère régulièrment (car elle contient des MéchantsPasBoVilains). Pour cela on ajoute à la section **Golbal settings** la ligne suivante: 
~~~bash
server_names = ['ibkstrum', 'cryptostorm', 'bortzmeyer', 'DNS_opennic-R4SAS']
~~~

Il reste à sauvegarder et fermer le fichier de configuration puis redemarrer DNScrypt-proxy:
~~~bash
sudo systemctl restart dnscrypt-proxy.service
~~~
Si nous avons aucun retour de message dans le bash c'est bon signe, notre fichier de conf est conforme et n'empêche pas le démarrage de DNScrypt.

#### Changement du port d'usage de DNScrypt

Par défaut, le port 53 est celui utilisé par le système pour les requêtes DNS.
Comme DNScrypt sera chargé de faire ces requêtes on lui attribue l'usage de ce port 53.

`sudo mousepad /etc/dnscrypt-proxy/dnscrypt-proxy.toml`

Nous allons au début du fichier à la section **Global settings**, on décommente la ligne `## listen_addresses = ['127.0.0.1:53', '[::1]:53']` pour avoir
~~~bash
listen_addresses = ['127.0.0.1:53', '[::1]:53']
~~~
Cependant Unbound utilise le même port, il conviendra aussi de modifier son fichier de conf pour éviter tout conflit.


#### Configuration de Unbound avec DNScrypt

Maintenant que nous utilisons DNScrypt, il faut ajuster la configuration de Unbound.

Premièrement, à la section **forward-zone:**, nous demandons à Unbound de ne plus aller faire des requêtes sur des résolveurs extérieurs mais auprès de DNScrypt.

Nous éditons le fichier de conf de Unbound:
~~~bash
sudo mousepad /etc/unbound/unbound.conf
~~~

Il convient de commenter les lignes pour les résolveurs que nous avions précédemment renseignés et ajoutons l'adresse locale sur le port utilisé par DNScrypt.

~~~bash
    do-not-query-localhost: no
forward-zone:
	name: "."
	#forward-addr: 109.230.224.42  # orsn-ns.godau.eu
	#forward-addr: 80.67.169.12  # serveur de FDN 2001:910:800::12
	#forward-addr: 80.67.169.40  # serveur de FDN
	#forward-addr: 208.67.222.222   #serveur DNS OpenDNS
	#forward-addr: 208.67.220.220  #serveur DNS OpenDNS
	forward-addr: 127.0.0.1@53  # dnscrypt-proxy
    forward-addr: ::1@53 # # dnscrypt-proxy
~~~

Deuxièmement, il faut changer le port d'usage par défaut de Unbound qui est 53 pour une autre valeur arbitraire comme 5353. Il ne faut pas prendre un port déjà utilisé par notre système.

Par exemple, sous la section **server** de unbound.conf nous choisissons arbitrairement le port 5353
~~~bash
server:
  port: 5353
  ~~~

### Configuration pour accroître la sécurité et la vitesse ~~hummm... mouai pas sûr hein!?~~
J'ai ajouté à ma configuration des paramètres suivants tirés de plusieurs sources pour renforcer la sécurité du cache Unbound avec DNScrypt. Mes compétences s'arrêtent là je ne peux vous expliquer chacun des paramètres. Peut-être certains d'entre vous pourraient le faire dans les commentaires.

~~~bash
server:
	aggressive-nsec: yes
	harden-below-nxdomain: yes
	num-threads: 2
	msg-cache-slabs: 4
	rrset-cache-slabs: 4
	infra-cache-slabs: 4
	key-cache-slabs: 4
	rrset-cache-size: 100m
	msg-cache-size: 50m
	outgoing-range: 465
	so-rcvbuf: 4m
	so-sndbuf: 4m
	port: 5353
	do-ip4: yes
	do-ip6: yes
	do-udp: yes
	do-tcp: yes
	do-daemonize: yes
	hide-identity: yes
	hide-version: yes
	harden-glue: yes
	harden-dnssec-stripped: yes
	harden-referral-path: yes
	harden-short-bufsize: yes # ajouté depuis https://forums.freebsd.org/threads/howto-jailed-unbound-dnscrypt-proxy-with-dnssec.48966/
  	harden-large-queries: yes # ajouté depuis https://forums.freebsd.org/threads/howto-jailed-unbound-dnscrypt-proxy-with-dnssec.48966/
	use-caps-for-id: yes
	prefetch: yes

	private-address: 127.0.0.1
  ~~~


# Configuration et protection du fichier resolv.conf
Dernière étape, il convient de modifier les paramètres de DNS du fichier de configuration resolv.conf et de le protéger comme les modifications faites par Network-Manager. Pour empêcher Network-Manager cette modificaion il y a deux moyens le premier est d'empêcher la modification du fichier par la commande.

## Première méthode:
Nous emêchons toute modif du fichier.
~~~bash
chattr +i /etc/resolv.conf
~~~
Pour revenir en arrière:
~~~bash
chattr -i /etc/resolv.conf
~~~

## La seconde méthode:
Nous utilisons une application openresolv qui assure que les paramètres du resolv.conf soient tels que nous le souhaitons à chaque démarrage.

~~~bash
 sudo pacman -S openresolv
 ~~~

 Nous éditons le fichier resolvconf.conf:

 ~~~bash
 sudo mousepad /etc/resolvconf.conf
 ~~~
en copiant ceci
 ~~~bash
# Configuration for resolvconf(8)
# See resolvconf.conf(5) for details

resolv_conf=/etc/resolv.conf
# If you run a local name server, you should uncomment the below line and
# configure your subscribers configuration files below.
#name_servers=127.0.0.1

# If you run a local name server, you should uncomment the below line and
# configure your subscribers configuration files below.
name_servers="::1 127.0.0.1"
options edns0
# private_interfaces="*"
~~~
 Ensuite on applique ces paramètres sur le fichier /etc/resolv.conf : `sudo resolvconf -u`

# Oufff c'est la fin

Nous redémarrons unboud et DNSCrypt.
~~~bash
sudo systemctl restart dnscrypt-proxy.service && sudo systemctl restart dnscrypt-proxy.service
~~~

et rendons les deux services actifs au démarrage.

~~~bash
sudo systemctl enable dnscrypt-proxy.service && sudo systemctl enable unbound.service
~~~

Nous pouvons faire une seconde vérification en interrogeant le statut de dnscrypt-proxy:

~~~bash
sudo systemctl status dnscrypt-proxy.service
[sudo] Mot de passe de user :
● dnscrypt-proxy.service - DNSCrypt-proxy client
     Loaded: loaded (/usr/lib/systemd/system/dnscrypt-proxy.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2020-11-19 09:08:57 CET; 25min ago
       Docs: https://github.com/jedisct1/dnscrypt-proxy/wiki
   Main PID: 611 (dnscrypt-proxy)
      Tasks: 10 (limit: 9398)
     Memory: 24.7M
     CGroup: /system.slice/dnscrypt-proxy.service
             └─611 /usr/bin/dnscrypt-proxy --config /etc/dnscrypt-proxy/dnscrypt-proxy.toml

Nov 19 09:10:37 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:37] [NOTICE] [cryptostorm] OK (DNSCrypt) - rtt: 49ms
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] [ibkstrum] OK (DNSCrypt) - rtt: 39ms
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] [DNS_opennic-R4SAS] OK (DNSCrypt) - rtt: 46ms
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] [bortzmeyer] OK (DoH) - rtt: 34ms
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] Sorted latencies:
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] -    34ms bortzmeyer
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] -    39ms ibkstrum
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] -    46ms DNS_opennic-R4SAS
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] -    49ms cryptostorm
Nov 19 09:10:38 user-portable dnscrypt-proxy[611]: [2020-11-19 09:10:38] [NOTICE] Server with the lowest initial latency: bortzmeyer (rtt: 34ms)
~~~

Ici on peut voir si  la connexion se fait bien avec les résolveur DNS que nous avons définis précédemment dans le fichier de conf dnscrypt-proxy.toml. Les temps de latence sont aussi notés entre 30 et 40ms c'est pas si mal. Tout va bien. Grâce au cache Unbound si nous allons sur un site déjà visité, unbound se passe de DNScrypt-proxy et va chercher l'adresse IP stockée dans son cache avec un temps de latence de 0ms !

Première requête: temps **23 ms**
~~~bash
$ drill wikimedia.org
;; ->>HEADER<<- opcode: QUERY, rcode: NOERROR, id: 8387
;; flags: qr rd ra ; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0
;; QUESTION SECTION:
;; wikimedia.org.	IN	A

;; ANSWER SECTION:
wikimedia.org.	2399	IN	A	91.198.174.192

;; AUTHORITY SECTION:

;; ADDITIONAL SECTION:

;; Query time: 23 msec
;; EDNS: version 0; flags: ; udp: 4096
;; SERVER: 127.0.0.1
;; WHEN: Thu Nov 19 13:28:44 2020
;; MSG SIZE  rcvd: 58
~~~
Seconde requête: **0 ms**
~~~bash
[user@user-portable ~]$ drill wikimedia.org
;; ->>HEADER<<- opcode: QUERY, rcode: NOERROR, id: 2538
;; flags: qr rd ra ; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0
;; QUESTION SECTION:
;; wikimedia.org.	IN	A

;; ANSWER SECTION:
wikimedia.org.	2397	IN	A	91.198.174.192

;; AUTHORITY SECTION:

;; ADDITIONAL SECTION:

;; Query time: 0 msec
;; EDNS: version 0; flags: ; udp: 4096
;; SERVER: 127.0.0.1
;; WHEN: Thu Nov 19 13:28:47 2020
;; MSG SIZE  rcvd: 58
~~~


Voilà maintenant nous sommes moins plus vulnérables à la censure d'un bloqueur de DNS, protégé contre le DNS spoofing, et on donne moins facilement notre historique de navigation à notre FAI. Enfin grâce au cache Unbound la navigation est un peu plus rapide. Attention ceci ne rend pas notre navigation anonyme comme le fait Tor.

Toute modification, correction et commentaire au tuto sont bienvenus!

Ce tuto est construit à partir d'informations trouvées ici et là:

https://wiki.archlinux.org/index.php/Dnscrypt-proxy

https://github.com/DNSCrypt/dnscrypt-proxy

https://www.linuxsecrets.com/archlinux-wiki/wiki.archlinux.org/index.php/DNSCrypt.html

https://www.redpill-linpro.com/techblog/2019/08/27/evaluating-local-dnssec-validators.html#private-domains-and-negative-trust-anchors

https://www.bortzmeyer.org/unbound.html

https://dnscrypt.info/public-servers/

https://wiki.evolix.org/HowtoUnbound

https://medium.com/nlnetlabs/aggressive-use-of-the-dnssec-validated-cache-in-unbound-1ab3e315d13f

https://www.reddit.com/r/dnscrypt/comments/iacw2u/can_unbound_be_used_together_with_dnscryptproxy/

#navigation #privacy #DNScrypt #Unbound #DNS_spoofing #Archlinux #vieprivée #sécurité
